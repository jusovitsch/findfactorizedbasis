(* ::Package:: *)

Print["Get Denominators in Factorized Form 1.0"];
Print["by Johann Usovitsch"];

BeginPackage["gD`"]

getNewBasis::usage = "getNewBasis[topo_,variables_,pathToKira_,d_,sampleSeed_] finds a new basis of master integrals, where all dependence in d factorizes in the denominators of the final reduction. This tool launches Kira in the background. See the documentation in the doc folder.";
generateSamples::usage = "generateSamples[variables_, topo_, pathToKira_, level_,checkPoint_] for debug usage only";
collectCoef::usage = "collectCoef[inputFile_] for debug usage only";
initiateUnknowns::usage = "initiateUnknowns[newsInput] for debug usage only";
prepareMap::usage = "prepareMap[news_] for debug usage only";
getNextUnknown::usage = "getNextUnknown[coefNumber_,mapInput_] for debug usage only";
getListOfSectors::usage = "getListOfSectors[probe_] for debug usage only";
generatePossibleMI::usage = "generatePossibleMI[input_] for debug usage only";
MIreplacement::usage = "MIreplacement[int_,currentSector_,allRules1_,allRules2_,topo_,d_] for debug usage only";
produceProbe::usage = "produceProbe[topo_, tables_,extrarule_]";
getSteps::usage = "getSteps[masterPot_, ibps_, rememberStepsIn_]";

safe;
progress = 0;

Begin["`Private`"]

(************************************************)


progressBar[Dynamic[x_]] := DynamicModule[{texture},
   texture = 
    Table[ColorData["Rainbow", t] /. RGBColor -> List, {t, 0, 50, 
      1}];
   Graphics[{
     (*Texture@texture, *)
     Polygon[{{0, 0}, {0, 1}, {50, 1}, {50, 0}}
      (*,VertexTextureCoordinates -> {{0}, {0}, {50}, {50}}*)]
     ,GrayLevel@.9, Dynamic@Polygon[{{x, 0}, {x, 1}, {50, 1}, {50, 0}}]
     },
    PlotRange -> {{0, 50}, {0, 1}}, PlotRangePadding -> 0,
    ImagePadding -> {{5, 5}, {18, 18}}, ImageSize -> {300, 70}
     ,AspectRatio -> 0.1
     ,Frame -> True, FrameTicks -> {{None, None}, {All, True}} 
    ]];

generateSamples[variables_, topo_, pathToKira_, level_,checkPoint_] := Block[{initPrime,tule,s1,s2,runKira,tables,allInts,toCompare,probe,count,a4,b4,fname,checkValue,sector},
(*293 interesting mathematical property*)
  initPrime = 1293;
  Do[initPrime = NextPrime[initPrime], {i, level}];

  tule = Table[{i, initPrime = NextPrime[initPrime]}, {i,
     variables}];
  (*Print[tule];*)

  sector = "";
  If[FileExistsQ["problematicSectors"]
    ,
    problematicSectors = ReadList["problematicSectors"];
    If[Length[problematicSectors]>0,
      sector = (" --set_sector="<>ToString[problematicSectors[[-1]]]<>" ");
    ];
  ];

  s1 = pathToKira;
  s2 = Table[
    " -s" <> ToString[tule[[i]][[1]]] <> "=" <>
     ToString[tule[[i]][[2]]], {i, Length[tule]}];

  runKira = Flatten[StringSplit[#," "]&/@{s1, s2, sector}];

  Print[runKira];
  fname = (checkPoint<>".m");

  Print["Create backup file: ",fname];

  If[FileExistsQ[fname],
    checkValue = ReadList[fname];

    If[ToString[checkValue[[1]]]==="go",
      Print["Resume reduction"];
      Print["running Kira!"];
      Print[runKira];
      RunProcess[runKira];
      Print[("mv "<>"results/"<>ToString[topo]<>"/kira_" <> ToString[topo] <> ".m "<>fname)];
      Run[("mv "<>"results/"<>ToString[topo]<>"/kira_" <> ToString[topo] <> ".m "<> fname)];
    ]

  ,
    Print["***Reduction process***"];
    Print["rm -r tmp/ results/ sectormappings/"];
    Run["rm -r tmp/ results/ sectormappings/"];

    o = OpenWrite[fname];
    WriteLine[o,ExportString["go","Table"]];
    Close[o];

    RunProcess[runKira];
    Print[("mv "<>"results/"<>ToString[topo]<>"/kira_" <> ToString[topo] <> ".m "<> fname)];
    Run[("mv "<>"results/"<>ToString[topo]<>"/kira_" <> ToString[topo] <> ".m "<> fname )];

    (*checkmasters = ReadList["results/"<>ToString[topo]<>"/masters"]/.b_*topo[a__]->topo[a];*)
  ];


  Print["Read reduction results from: ", fname];

  tables = Import[fname];
  Return[tables];
];

produceProbe[topo_, tablesIn_,extrarule_,num_] := Block[{initPrime,tule,s1,s2,runKira,allInts,toCompare,probe,count,a4,b4,fname,checkValue,sector},

  flagTableZero = 1;
  done = {};
  doneCoef = {};
  tables = tablesIn;
  tables = DeleteCases[tables, Rule[topo[__], 0]];

  (*Print["PROBLEM1"];*)
  If[Length[tables]==0,
    Return[0];
  ];
  (*Print["PROBLEM2"];*)

  ints=tables[[num]];

  (*Print[ints, num];*)

  (*Do[*)
    If[ints[[2]]=!=0,

      ints[[2]] = Factor/@(ints[[2]]//.extrarule//Collect[#,topo[__]]&);

      allInts =
      If[MatchQ[ints[[2]], Plus[_, __]],
        Cases[(List @@ ints[[2]]), b__*topo[a__] -> topo[a]]
        ,
        {ints[[2]]} /. b__*topo[a__] -> topo[a]
      ];

      list = If[MatchQ[ints[[2]], Plus[_, __]]
        ,
        List@@ints[[2]]
        ,
        {ints[[2]]}
      ];

      positionMI = Position[allInts,Alternatives@@Complement[allInts,done]]//Flatten;
      done = Join[done,allInts[[positionMI]]];

      doneCoef = Join[doneCoef,list[[positionMI]]];

      If[Length[done] == Length[checkmasters],
        flagTableZero = 0;
        (*Break[];*)
      ];
    ];
  (*,{ints,Reverse[tables]}];*)

  toCompare =
  Table[
    safe[
      done[[i]]
    ,
      doneCoef[[i]] // Denominator // Factor]
  , {i, Range[Length[done]]}]//Sort;

  If[flagTableZero,
    Print["No integrals were reduced"];
    Return[{0,0}];
  ];

  If[Length[toCompare]==0,
    Print["Sector with no master integrals."];
    Return[{0,0}];
  ];

  probe =
   Sort[toCompare, (

      count = 0;
      a4 =
       Plus @@ Table[
         If[i > 0, count++; 2^(count - 1),
          count++;0], {i, #1 /. safe[topo[a__], b_] -> {a}}];

      count = 0;
      b4 =
       Plus @@ Table[
         If[i > 0, count++; 2^(count - 1),
          count++;0], {i, #2 /. safe[topo[a__], b_] -> {a}}];

      a4 < b4) &];

  Return[probe];
];

(************************************************)

collectCoef[inputFile_] := Block[{news,coef},


  news = Table[
    coef = (inputFile[[it]] /. safe[__, a_] -> a);

    If[MatchQ[coef /. safe[__, a_] -> a, Times[_, __]]
    ,
      List @@ coef
    ,
    {coef}
    ]
  , {it, Length[inputFile]}];


  Return[news];
];

(************************************************)

getNextUnknown[coefNumber_,mapInput_]:=Block[{it,range,token,map},

  map = mapInput;

  it = coefNumber;

  token = map[[it]];

  range = Range[Length[map]];

  range = DeleteCases[range, it];

  Do[token = Complement[token, map[[i]]], {i, range}];

  Return[token];

];

(************************************************)

prepareMap[news_]:=Block[{rules,value,power,map},

(*Power[Plus[a_,b__],c_]->(a+b)*)

  rules = Table[
    value = news[[1, j]];

    If[! NumericQ[value],
      power =
        If[MatchQ[value, Power[a_,b_]], value /. Power[a_,c_]->c, 1];
      value =
        If[MatchQ[value, Power[a_,b_]], value /. Power[a_,c_]->a, value];

      value -> {{1, power}}
      ,
      0
    ]
  , {j, Length[news[[1]]]}];

  rules = DeleteCases[rules, 0];

  map = Association[rules];

  Do[
    Do[
      value = news[[i, j]];
      If[NumericQ[value], Continue[];];


      power =
        If[MatchQ[value, Power[a_,b_]], value /.Power[a_,c_]->c, 1];
      value =
        If[MatchQ[value, Power[a_,b_]], value /. Power[a_,c_]->a, value];



      If[Lookup[map, value, 0] =!= 0,
        map[value] = Append[map[value], {i, power}]
        ,
        map = Append[map, value -> {{i, power}}]
      ]
    ,{j, Length[news[[i]]]}]

  ,{i, 2, Length[news]}];

  keys = Keys[map];
  length = Length[Keys[map]];
  Do[
    Do[
      If[Length[map[[it]]] == Length[map[[jt]]],

        test = DeleteCases[(map[[it]] - map[[jt]]), {0, 0}];

        If[Length[test] == 0,

          map = Append[map, (keys[[it]]*keys[[jt]] // Simplify // Expand)->map[[it]]];
          map = KeyDrop[map, keys[[it]]];
          map = KeyDrop[map, keys[[jt]]];

          jt = jt - 1;
          length = length - 1;
          Break[];
        ];
      ];
    , {it, jt + 1, length}];
  , {jt, length - 1}];

  Return[map];

];

(************************************************)

initiateUnknowns[newsInput_] := Block[{map,final,positions,id,coef,ids,keys},

  news=newsInput;
  map = prepareMap[news];
  final = {};

  keys = Keys[map];

  ids = Table[
    positions = getNextUnknown[it, map];
    If[Length[positions] != 0,
      id = {positions[[1]],keys[[it]]}
    ]
  , {it,Length[Keys[map]]}];

  ids = DeleteCases[ids, Null];

  Return[{ids,map}];
];

(************************************************)

getListOfSectors[probe_] := Block[{topo,sects,count,int,listSects,tmpData},

  topo = Head[(probe /. safe[a_, __] -> a)[[1]]];
  sects = Table[
    count = 0;
    int = integral /. safe[topo[a__], b_] -> {a};
    {Plus @@ Table[If[i > 0, count++; 2^(count - 1), count++; 0]
       , {i, int}], int}
    , {integral, probe}] // Sort;

  biggestSector=sects[[-1]][[1]];

  listSects = Table[
    tmpData = Table[
      If[j == i[[1]],
       i
       ,
       0
       ]
    , {i, sects}];
    tmpData = DeleteCases[tmpData, 0];
    If[Length[tmpData] == 0,
     0
     ,
     tmpData
    ]
  , {j, biggestSector}];

  listSects = DeleteCases[listSects, 0];

  Return[{topo,listSects}];
]

(************************************************)

generatePossibleMI[input_] := Block[{listOfDots,count,sector,maskSector,init,MI},

  count = 0;
  sector = 0;
  maskSector =
  Table[
    If[i > 0,
      count++; sector += 2^(count - 1); count
      ,
      count++;0
    ]
  , {i, input}];

  init = Table[0,{i,Length[input]}];

  listOfDots = DeleteCases[maskSector, 0];
  listOfDots1 = Table[If[i==1,2,1] ,{i,Length[listOfDots]}];
  listOfDots1=listOfDots1//Permutations;

  listOfDots2 = Table[If[i==1||i==2,2,1] ,{i,Length[listOfDots]}];
  listOfDots2=listOfDots2//Permutations;
  listOfDots = Join[listOfDots1,listOfDots2];

  MI = Table[
    count = 0;
    Table[
      If[maskSector[[j]] > 0
        ,
        count++; dots[[count]]
        ,
        0
      ]
    , {j, Length[maskSector]}]
 , {dots, listOfDots}];

 Return[MI];
]

(************************************************)

prepare2compare[int_,allRules_,topo_,d_]:=Block[{repl,toCompare,collected,list,count,allInts,powerAll,indices,integralComplexity},

  repl = topo[int/.List[a__]->a];

  collected = Collect[repl /. allRules1, topo[__]];
  If[collected===0,
    Return[0];
  ];

  list = If[MatchQ[collected, Plus[_, __]]
    ,
    List@@collected
    ,
    {collected}
  ];

  If[Length[list]==1 && list[[1]]==repl,

    Return[0];
  ];

  allInts =
    Cases[list, Alternatives @@ {b__*topo[a__], topo[a__]} -> topo[a]];

  toCompare = Table[

    indices = {allInts[[i]] /. topo[a__] -> a};
    integralComplexity = Plus @@ ((If[# <= 0, Abs[#], (# - 1)]) & /@ indices);

    safe[
      allInts[[i]]
    ,
      (count = 0;
      Sum[If[ind > 0, count++; 2^(count - 1), count++;0], {ind, indices}])
    ,
      numeratorS=list[[i]]/.topo[__]->1// Numerator // Factor;
      powerAll=Exponent[numeratorS,d];
      numeratorS =
      If[MatchQ[numeratorS, Times[_, __]]
      ,
        List@@numeratorS
      ,
        {numeratorS}
      ];
      numeratorS=DeleteCases[numeratorS, a_ /; NumericQ[a]];
      If[Length[numeratorS]==0,
        numeratorS={1};
      ];
      {Max[Exponent[numeratorS,d]],powerAll,numeratorS,integralComplexity}
    ,
      list[[i]] // Denominator // Factor]

  , {i, Range[Length[allInts]]}]//Sort;

  Return[toCompare];
]


(************************************************)

MIreplacement[int_,currentSector_,allRules1_,allRules2_,topo_,d_]:=Block[{repl,numerator,denominator,toCompare,probe,collected,list,count,allInts,a4,b4,powerAll},

  toCompare = prepare2compare[int,allRules1,topo,d];
  toCompare2 = prepare2compare[int,allRules2,topo,d];

  If[Length[toCompare]==0 || Length[toCompare2]==0,
    Return[0];
  ];

  compNumCoef=Table[
    num1=Times@@(toCompare[[ints]]/.safe[_,_,a_,__]->a)[[3]];
    num2=Times@@(toCompare2[[ints]]/.safe[_,_,a_,__]->a)[[3]];
    If[Abs[num1/num2//Simplify]===1,
    {toCompare[[ints]]/.safe[a_,b_,c_,e_]->safe[a,b,Join[{1},c],e],
      toCompare2[[ints]]/.safe[a_,b_,c_,e_]->safe[a,b,Join[{1},c],e]}
    ,
    {toCompare[[ints]]/.safe[a_,b_,c_,e_]->safe[a,b,Join[{2},c],e],
      toCompare2[[ints]]/.safe[a_,b_,c_,e_]->safe[a,b,Join[{2},c],e]}
    ]
  ,{ints,Length[toCompare]}];

  toCompare = compNumCoef[[All,1]];

  probe = DeleteCases[toCompare, safe[a_,b_,__] /; currentSector != b];

  If[Length[probe]==0,
    Print["No match for the master integral replacement: ", int];
    Print["Continue with the next master integral"];
    Return[0];
  ];

  probe = SortBy[probe, {#[[2]], #[[3, 1]], #[[3, 2]], #[[3, 3]], #[[3, 4]]} &];

  Return[probe];
]


getSteps[masterPot_, ibpsIn_, rememberStepsIn_] :=
  Block[{allRpl, numberOfMaster, originalmasters, iterStep, extrarule,
     sl, ig, chc, token1, token, jj, num, rememberSteps, delete,
    masterPotInt, ibps, initialMasters},

   Print["getSteps:"];
   If[Length[rememberStepsIn] != 0,
    rememberSteps = rememberStepsIn;
    mode = 1;
    ,
    rememberSteps = {};
    mode = 0;
   ];
   masterPotInt = masterPot;
   ibps = ibpsIn;   
   
   countPosMasterPot =0;
   rmPotentialM={};
   
   If[countPosMasterPot++;Factor[#-(# /. (ibps))]==0,rmPotentialM = Join[rmPotentialM,{{countPosMasterPot}}];]&/@masterPotInt;
   rmPotentialM=rmPotentialM//DeleteCases[#,{}]&;
   masterPotInt=Delete[masterPotInt,rmPotentialM];
   
   If[Length[masterPotInt]==0,Print["Probably r: level in the jobs.yaml file is to low."];Abort[];];
   
   allRpl = 
   (
   If[(# /. ibps)=!=0,
            {# - (# /. ibps)}
             , 
             0
     ]) & /@ masterPotInt // Flatten;
   allRpl = DeleteCases[allRpl,0];
   
   initialMasters=(({(# /. (ibps))}) & /@ masterPotInt // Flatten) /.
       ToExpression["d"] -> 373 // Variables;
       

   numberOfMaster = Length[initialMasters];

   ibps = DeleteCases[ibps, Rule[topo_[__], 0]];

   iterStep = ibps[[-1, 2]];
   (*Print[iterStep];*)
   topo = (iterStep/.ToExpression["d"]->373//Variables)[[1]]//Head;


    token1 = Collect[(iterStep), topo[__]];
    If[MatchQ[token1, Plus[_, __]],
      token1 = Factor /@ token1;
      If[MatchQ[token1, Plus[_, __]],
        token = List @@ (Factor /@ token1);
        ,
        token = {token1}
      ];
    ,
    token = {Factor[token1]}
    ];

    If[mode == 0,
      aha = {
        Plus @@ (token // Denominator // Exponent[#, ToExpression["d"]] &),

        Plus @@ (token /. topo[__] -> 373 // Numerator //
          Exponent[#, ToExpression["d"]] &),
        (token // Numerator // ToString // StringLength),
        0,
        0,
        (token /. topo[__] -> 1 // Numerator) //
        Exponent[#, ToExpression["d"]] &,
        (token // Denominator // Exponent[#, ToExpression["d"]] &),
        0
      };
      rememberSteps = Join[rememberSteps, {aha}];
    ];

   extrarule = {1 -> 1};

   Print["Number of potential new master integral candidates: ",numberOfMaster];
   
   If[progress==0,
     Print[progressBar[Dynamic[oo]]];
     progress=1;
   ];
   Do[
    allRpl =
     allRpl //. extrarule // Collect[#, topo[__]] & // Simplify //
      DeleteCases[#, 0] &;
    If[Length[allRpl] == 0,
     Break[];];
    originalmasters = (iterStep /. ToExpression["d"] -> 373) //
      Variables;
    If[oo==1,
      Print["Number of master integrals:", Length[originalmasters]];
    ];
    sl = Table[
      Solve[# == 0, ig] & /@ allRpl // Flatten, {ig, originalmasters}];
    If[mode == 0,
     chc = Table[
         Table[
          token1 = Collect[(iterStep /. sl[[jj, num]]), topo[__]];
          If[MatchQ[token1, Plus[_, __]],
           token1=(Factor /@ token1);
           If[MatchQ[token1, Plus[_, __]],
             token = List @@ token1;
             ,
             token = {token1};
           ];
           ,
           token = {Factor[token1]}
          ];
          {
           Plus @@ (token // Denominator // Exponent[#, ToExpression["d"]] &),

           Plus @@ (token /. topo[__] -> 1 // Numerator //
              Exponent[#, ToExpression["d"]] &),
           (token // Numerator // ToString // StringLength),
           jj,
           num,
           (token /. topo[__] -> 1 // Numerator) //
            Exponent[#, ToExpression["d"]] &,
           (token // Denominator // Exponent[#, ToExpression["d"]] &),
           oo
           }
          , {num, Length[sl[[jj]]]}]
         , {jj, Length[sl]}] // Flatten[#, 1] & // Sort;
     chcT = DeleteCases[chc, {__, a_,_} /; Length[a] < numberOfMaster];

     (*While[Length[chcT] == 0 &&numberOfMaster != 0,
       chcT = DeleteCases[chc, {__, a_,_} /; Length[a] < numberOfMaster--];
     ];*)
     chc = chcT;

     If[Length[chc]==0, Print["Probably the r: value in the jobs.yaml fiel is chosen to low"]; Break[];];
     
     rememberSteps = Join[rememberSteps, {chc[[1]]}];
     ,
     If[oo+1>Length[rememberSteps],Print["Probably the r: value in the jobs.yaml fiel is chosen to low"]; Break[];];
     chc = rememberSteps[[oo+1]];
     chc[[1]] = chc;
     ];

     If[Length[chc]==0, Print["Probably the r: value in the jobs.yaml fiel is chosen to low"]; Break[];];

     rpl1 =
     Collect[(sl[[chc[[1, 4]], chc[[1, 5]]]][[2]]),
      topo[__]];
    If[MatchQ[rpl1, Plus[_, __]],
     rpl2 = Factor /@ rpl1;
     ,
     rpl2 = Factor[rpl1];
    ];
    rpl2=sl[[chc[[1, 4]], chc[[1, 5]]]][[1]]->rpl2;

    token1 =
     Collect[(iterStep /. rpl2),
      topo[__]];
    If[MatchQ[token1, Plus[_, __]],
     iterStep = Factor /@ token1;
     ,
     iterStep = Factor[token1];
    ];
    extrarule =
     Join[extrarule, {rpl2}];
    delete =
     Complement[
       sl[[1 chc[[1, 4]]]][[chc[[1, 5]]]][[2]] /. ToExpression["d"] -> 373 // Variables,
        originalmasters][[1]];
    masterPotInt = DeleteCases[masterPotInt, delete];
    If[Length[masterPotInt] == 0,
     Break[];
     ];
    , {oo, 50}];
    If[mode==0,
      rememberStepsSorted = rememberSteps// SortBy[#, {#[[1]], #[[2]], #[[3]]}] &;
    ];
   Return[{rememberSteps, extrarule,rememberStepsSorted,initialMasters}];

];


getNewBasis[topo_,variables_,pathToKira_,d_,sectorSeek_,sampleSeed___] := Block[{count,collectFinishedSectors,probe1,allRules1,probe2,allRules2,ids1,map1,ids2,map2,problems,topoX,listOfSectos,iterationLength,preferredMI,fname,input,testTmp,possibleMI,masterInts,denominator,problematicSectors,resultsA,resultsB,nextReductionSector,currentSector,masterints,ints,sects,lengthInt,trim,choiceMI,zerosFile,rememberSteps,rememberSteps2,pathToKiraX,posInit,it},

  progress=0;
  testIntegralfamilies = ImportString[ StringReplace[
   Import["config/integralfamilies.yaml",
    "Text"], {StartOfLine ~~ "#" ~~ Shortest[___] ~~ EndOfLine ~~
      "\n" -> "", "#" ~~ Shortest[___] ~~ EndOfLine -> ""}], "Table"];

  posInit = Position[testIntegralfamilies, "propagators:"][[1, 1]];
  posInitEnd = posInit;
  Do[
    If[it < posInitEnd + 1, Continue[]]; 
    If[it != posInitEnd + 1, Break[];];
    posInitEnd = it;
  , {it, Flatten[Position[testIntegralfamilies[[All, 1]], "-"]]}
  ];
  lengthInt=posInitEnd-posInit;
  Print["Number of propagators detected: ",lengthInt];
  
  
  If[Length[Cases[testIntegralfamilies, {"-", "name:", a__} -> a]]=!=1,
    Print["You have defined more than one topology in config/integralfamilies.yaml."];
    Print["Right now only one topology is supported."];
    Return[];
  ];

  Print["If it is a new run, remove the backup files resultsA.m and resultsB.m in the working directory. And resume the run."];
  
  o=OpenWrite["trivial.yaml"];
  stringTrivialJob =  "jobs:\n"<>
                      " - reduce_sectors:\n"<>
                      "    reduce:\n"<>
                      "     - {r: "<>ToString[lengthInt]<>", s: 2}\n"<>
                      "    run_triangular: sectorwise";
  WriteString[o,stringTrivialJob];
  Close[o];



  Print["Get trivial Sectors"];
  Run["rm -r tmp results/ sectormappings"];
  
  RunProcess[{pathToKira[[1]], "-i2", "trivial.yaml", "-p4"}, "StandardOutput"];

  zerosFile = ("{" <> Import["sectormappings/"<>ToString[topo]<>"/trivialsector", "Text"] <>
    "}") // ToExpression;


  If[FileExistsQ["problematicSectors"],
    allSectorsRemained = ReadList["problematicSectors"];
    If[Length[allSectorsRemained]==0,
      Print["New Basis is written to the file preferred: ", ReadList["preferred"] ];
      Print["If the list is empty, no replacements were needed"];
      Return[];
    ];
    ,
    allSectorsRemained = Complement[i = 0; Table[i++, sectorSeek+1], zerosFile];
    o = OpenWrite["problematicSectors"];
    Do[
      WriteLine[o,ToString[ii]];
    ,{ii,allSectorsRemained}];
    Close[o];
  ];



  count = 0;

  While[count < 700,

    If[Length[allSectorsRemained]==0,
      Break[];
    ];

    Print["Check sector: ",allSectorsRemained[[-1]]];
    
    pos = Position[
    PadRight[Reverse[IntegerDigits[allSectorsRemained[[-1]], 2]], 
     lengthInt], 1] // Flatten;
     
    skelleton = Table[1, {Length[pos]}];
    
    skelletonModify = skelleton;
    If[Length[pos] > 0, skelletonModify[[1]] = 2;
      perm = Permutations[skelletonModify, {Length[pos]}];];
    If[Length[pos] > 1, skelletonModify[[2]] = 2;
      perm = Join[perm, Permutations[skelletonModify, {Length[pos]}]];
      skelletonModify[[2]] = 1;
      (*skelletonModify[[1]]=3;
      perm=Join[perm,Permutations[skelletonModify,{Length[pos]}]];*)];
    perm = Join[{skelleton}, perm];
    
    masterPot = 
      Table[PadRight[SparseArray[pos -> oo] // Normal, lengthInt], {oo, 
        perm}];
    masterPot = topo[#] /. List[a__] -> a & /@ masterPot;
    
    pos = Position[masterPot[[1]], 0] // Flatten;
    
    skelleton = Table[0, {Length[pos]}];
    
    skelletonModify = skelleton;
    If[Length[pos] > 0, skelletonModify[[1]] = -1;
      perm = Permutations[skelletonModify, {Length[pos]}];];
    perm = Join[{skelleton}, perm];

    masterPot = 
    Table[Table[
      masterPot[[jj]] // 
       ReplacePart[#, 
         Table[pos[[ii]] -> perm[[kk]][[ii]], {ii, 
           Length[pos]}]] &, {jj, Length[masterPot]}], {kk, 
      Length[perm]}] // Flatten // DeleteDuplicates;

    count++;
    Print["*** Iteration step: ", count," ***"];
    (*Get two samples to find kinematic dependance of denominators*)


    If[FileExistsQ["problematicSectors"] && FileExistsQ["preferred"],

      (*ints = ReadList["preferred"];*)
      sects = ReadList["problematicSectors"];

      If[ (*Length[ints]>0 &&*) Length[sects]>0,

        (*lengthInt = Length[ints[[1]]];*)
        trim = topo[PadRight[ Reverse[IntegerDigits[sects[[-1]], 2]],lengthInt]/.List[a__]->a];

        o = OpenWrite["trimBasis"];
        WriteLine[o,ExportString[trim,"Table"]];
        Close[o];
      ];
    ];

    runCommandKira = {pathToKira[[1]],StringSplit[#," "]&/@pathToKira[[2;;-1]]}//Flatten;
    allRules1 =
    generateSamples[variables, topo, runCommandKira, sampleSeed+0,"resultsA"];

    probe1 = produceProbe[topo,allRules1,{1->1},-1];
(*     Print[probe1]; *)

    If[probe1 == 0,
      allSectorsRemained = DeleteCases[allSectorsRemained, sects[[-1]]];
      o = OpenWrite["problematicSectors"];
      Do[
        WriteLine[o,ExportString[allSectorsRemained[[i]],"Table"]];
      ,{i,Length[allSectorsRemained]}];
      Close[o];
      If[FileExistsQ["resultsA.m"],
        Print["remove backup result: resultsA.m"];
        Run["rm resultsA.m"];
      ];
      If[FileExistsQ["resultsB.m"],
        Print["remove backup result: resultsB.m"];
        Run["rm resultsB.m"];
      ];
      Continue[];
    ];

    {rememberSteps, extrarule1, rememberStepsSorted, oldMaster} = getSteps[masterPot, allRules1, 1];


    allRules2 =
    generateSamples[variables, topo, runCommandKira, sampleSeed+3,"resultsB"];
    (*allRules2 = DeleteCases[allRules2, Rule[topo[__], 0]];*)

    {rememberSteps2, extrarule2, rememberStepsSorted2, oldMaster} = getSteps[masterPot, allRules2, rememberSteps];


    startTest = 1;
    allRules1 = DeleteCases[allRules1, Rule[topo[__], 0]];
    allRules2 = DeleteCases[allRules2, Rule[topo[__], 0]];

    probeS={};
(*     Print["old masters: ",oldMaster]; *)

    Print["Number of relations to be tested: ", Length[allRules1]];
    Do[
      checkIt = rememberStepsSorted[[startTest]][[-1]];

      If[itTable==1,
        ww1 = oldMaster;
        ww2 = oldMaster;
      ];

      ww1 = Factor /@ # & /@ Collect[ww1 /. extrarule1 [[startTest]], topo[__]];
      ww2 = Factor /@ # & /@ Collect[ww2 /. extrarule2[[startTest]], topo[__]];
      ruleDo1=Table[oldMaster[[iti]]->ww1[[iti]],{iti,Length[oldMaster]}];
      ruleDo2=Table[oldMaster[[iti]]->ww2[[iti]],{iti,Length[oldMaster]}];

      probe1 =
        produceProbe[topo, allRules1, ruleDo1, itTable];
      probe2 =
        produceProbe[topo, allRules2, ruleDo2, itTable];
      news1 = collectCoef[probe1];
      {ids1, map1} = initiateUnknowns[news1];
      news2 = collectCoef[probe2];
      {ids2, map2} = initiateUnknowns[news2];
      (*Print[map1," ",map2];*)
      problems = (Complement[map1, map2] // Normal)[[All, 1]] // Factor;
      If[Length[problems] != 0,
(*         WriteString["stdout", problems]; *)
        startTest++;
        itTable = 1;
      ];
      If[Length[probe1] > Length[probeS],
        probeS = probe1;
      ];
      If[startTest>50,Print["No good case"];Abort[];];
    , {itTable, Length[allRules1]}];

(*     Print["Chosen set of new master integrals: ", checkIt+1]; *)
    Print["new set of master integrals for the current sector is: ",probeS];

    newMaster = probeS /. safe[a_, _] -> a;

    If[FileExistsQ["preferred"],
      preferredMI = ReadList["preferred"];
      ,
      preferredMI={};
    ];
    o = OpenWrite["preferred"];
    Do[
      WriteLine[o,ToString[master]];
    ,{master,newMaster}];

    Do[
      WriteLine[o,ToString[master]];
    ,{master,preferredMI}];
    Close[o];

    (*probe2 = produceProbe[topo, allRules2,{1->1},1];*)

    (*collect all denominators*)
    news1 = collectCoef[probe1];
    {ids1, map1} = initiateUnknowns[news1];
    news2 = collectCoef[probe2];
    {ids2, map2} = initiateUnknowns[news2];


    Print["***check master integrals***"];

    If[FileExistsQ["preferred"],
      preferredMI = ReadList["preferred"];
    ,
      preferredMI = {};
      fname = "preferred";
      o = OpenWrite[fname];
      Close[o];
    ];

    Print["Length of preferred integrals: ", Length[preferredMI]];

    {topoX, listOfSectos} = getListOfSectors[probe1];

    If[FileExistsQ["problematicSectors"],
      allSectorsRemained = ReadList["problematicSectors"];
    ,
      o = OpenWrite["problematicSectors"];
      Do[
        WriteLine[o,ExportString[listOfSectos[[i,1, 1]],"Table"]];
      ,{i,Length[listOfSectos]}];
      Close[o];

      allSectorsRemained=listOfSectos[[All,1, 1]];
    ];

    (*Print["sectors wait to be checked: ",allSectorsRemained];*)

    (*generate problematic denominator building blocks*)

    (*Print[map1];
    Print[map2];*)
    problems = (Complement[map1, map2] // Normal)[[All, 1]] // Factor;
    problems =
    Table[(If[MatchQ[i, Times[_, __]], List @@ i, i]), {i, problems}] //
      Flatten;

    Print[problems];

    Print["Problematic denominator building blocks: ", Length[problems]];
    If[Length[problems] == 0 && Length[allSectorsRemained]==0,
      Print["Factorization complete"];
      Break[];
    ];


    iterationLength = Length[listOfSectos];
    Print["Sectors left to scan: ", iterationLength];

    (*Perform replacements*)
    mastersReplaced = {};
    Do[
      problematicSectors = 0;
      currentSector = listOfSectos[[-integSector]][[1, 1]];
      input = listOfSectos[[-integSector]][[1, 2]];

      possibleMI = generatePossibleMI[input];

      masterInts = probe1/.safe[a_, b_]->a;

      masterInts = Complement[masterInts,mastersReplaced];
      masterInts = (Join[masterInts,preferredMI]//Union)/.topo[a__]->{a};

      possibleMI = Complement[possibleMI, masterInts];

      If[Length[possibleMI] === 0, Print["next sector"]; Continue[];];

      allpossibleNewMI = Table[

        oldMasterList = MIreplacement[possibleMI[[j]], currentSector, allRules1, allRules2, topo, d];

        If[Length[oldMasterList]>0,

          Table[

            denominator = oldMaster/.safe[__,b_]->b;
            testTmp =
            Table[If[(Simplify[denominator/problems[[i]]] // Denominator) !=
                problems[[i]],1,0]
            , {i, Length[problems]}];

            If[Length[DeleteCases[testTmp, 0]] != 0,

              safe[{(oldMaster/.safe[__,b_,_]->b),oldMaster/.safe[a_,__]->a, topo[possibleMI[[j]]/.List[b__]->b]}]
              ,
              0
            ]

          ,{oldMaster,oldMasterList}]
        ,
          0
        ]

      , {j, Length[possibleMI]}];

      allpossibleNewMI = Flatten[allpossibleNewMI]/.safe[a_]->a;

      allpossibleNewMI = DeleteCases[allpossibleNewMI,0];

      allpossibleNewMI = SortBy[allpossibleNewMI, {#[[1, 1]], #[[1, 2]], #[[1, 3]], #[[1, 4]]} &];


      If[FileExistsQ["usedMI"],
        usedMI = ReadList["usedMI"];
      ,
        usedMI = {};
      ];


      allpossibleNewMI = allpossibleNewMI[[Ordering[allpossibleNewMI]]];

      (*Print[allpossibleNewMI];*)

      choiceMI=1;
      Do[

        If[Length[Cases[usedMI,allpossibleNewMI[[iterNewMI]][[2]]]]==0,

          usedMI = Append[usedMI,allpossibleNewMI[[iterNewMI]][[3]]];

          o = OpenWrite["usedMI"];
          Do[
            WriteLine[o,ExportString[iter,"Table"]];
            WriteLine[o, ""];
          ,{iter,usedMI}]
          Close[o];

          choiceMI = iterNewMI;
          Break[];
        ]

      ,{iterNewMI,Length[allpossibleNewMI]}];

      Print["choiceMI: ",choiceMI];


      (*Print[allpossibleNewMI];*)

      If[Length[allpossibleNewMI]>0,

        Print["Replaced one building block"];

        Print[allpossibleNewMI[[choiceMI,2]]," goes to ",allpossibleNewMI[[choiceMI,3]]];

        fname = "preferred";
        o = OpenWrite[fname];
        mastersReplaced=Append[mastersReplaced,allpossibleNewMI[[choiceMI,2]]];
        Do[
          If[topo[MI /. List[a__] -> a]=!=allpossibleNewMI[[choiceMI,2]]
            ,
            WriteLine[o,
              ExportString[topo[MI /. List[a__] -> a],"Table"]];
            WriteLine[o, ""];
            ,
            WriteLine[o,
              ExportString[allpossibleNewMI[[choiceMI,3]],"Table"]];
            WriteLine[o, ""];
          ];

        ,{MI,masterInts}];
        Close[o];

        preferredMI = ReadList["preferred"];
        problematicSectors = 1;
      ];

      Print["finished sector: ", currentSector];
      If[problematicSectors == 0,
        allSectorsRemained = DeleteCases[allSectorsRemained, currentSector];
        o = OpenWrite["problematicSectors"];
        Do[
          WriteLine[o,ExportString[allSectorsRemained[[i]],"Table"]];
        ,{i,Length[allSectorsRemained]}];
        Close[o];
      ];
    , {integSector, iterationLength}];

    If[Length[allSectorsRemained]==0,
      Break[];
    ];

    
    Print["We clean up"];
    If[FileExistsQ["resultsA.m"],
      Print["remove backup result: resultsA.m"];
      Run["rm resultsA.m"];
    ];
    If[FileExistsQ["resultsB.m"],
      Print["remove backup result: resultsB.m"];
      Run["rm resultsB.m"];
    ];

  ];

  Print["New Basis is written to the file preferred: ", ReadList["preferred"] ];
  Print["If the list is empty, no replacements were needed"];

]

End[];

EndPackage[]
